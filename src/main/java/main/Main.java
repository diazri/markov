package main;

import org.apache.commons.cli.*;

import lib.Model;
import static lib.Debug.dp;

public class Main {

	// The model we'll train.
	private static Model model;
	
	// Options (and defaults).
	private static int order = 2;
	private static int min = 1;
	private static int max = 100;
	private static int num = 5;
	private static String filename = "data/namesBoys.txt";
	
	public static void main(String[] args) {
		// Our commandline object for handling arguments.
		CommandLine cl;

		// Define command line options
		Options ops = new Options();

		Option opt = Option.builder("o")
				.longOpt("order")
				.hasArg()
				.desc("How far ahead will the markov model look? (Must be an integer greater than or equal to 2.)")
				.build();
		ops.addOption(opt);

		opt = Option.builder("i")
				.longOpt("min")
				.hasArg()
				.desc("What is the minimum length of a name? (Must be an integer.)")
				.build();
		ops.addOption(opt);

		opt = Option.builder("a")
				.longOpt("max")
				.hasArg()
				.desc("What is the maximum length of a name? (Must be an integer.)")
				.build();
		ops.addOption(opt);

		opt = Option.builder("n")
				.longOpt("number")
				.hasArg()
				.desc("How many names should we generate? (Must be an integer.)")
				.build();
		ops.addOption(opt);
		
		opt = Option.builder("f")
				.longOpt("filename")
				.hasArg()
				.desc("The complete path to the input file.")
				.build();
		ops.addOption(opt);
		
		ops.addOption("h", "help", false, "Show the help menu.");

		// Parse the command line options
		CommandLineParser par = new DefaultParser();
		try {
			cl = par.parse(ops, args);
		} catch (ParseException e) {
			System.out.println();
			e.printStackTrace();
			throw new IllegalArgumentException("ERROR: Invalid arguments.");
		}

		// If the user asked for help, print the help menu and return.
		if (cl.hasOption("h")) {
			HelpFormatter help = new HelpFormatter();
			help.printHelp("SearchApp", ops);
			return;
		}

		// If the user supplied args, use them.
		if (cl.hasOption("i")) {
			String s = cl.getOptionValue("i");
			min = Integer.valueOf(s);
		}

		if (cl.hasOption("a")) {
			String s = cl.getOptionValue("a");
			max = Integer.valueOf(s);
		}

		if (cl.hasOption("o")) {
			String s = cl.getOptionValue("o");
			order = Integer.valueOf(s);
		}
		
		if (cl.hasOption("n")) {
			String s = cl.getOptionValue("n");
			num = Integer.valueOf(s);
		}
		
		if (cl.hasOption("f")) {
			String s = cl.getOptionValue("f");
			filename = s;
		}

		System.out.println("Instantiating model . . .");
		
		// Instantiate our model.
		model = new Model(order, min, max);
		
		System.out.println("Training on " + filename + " . . .");
		
		// Train our model.
		model.train(filename);
		
		dp(model.toString());
		
		System.out.println("Generating " + num + " names . . .");
		
		// Generate a name & print it.
		for(int i = 0; i < num; i++) {
			String name = model.generate();
			System.out.println("Name " + (i + 1) + ": \t" + name);
		}
	}

}
