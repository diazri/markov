package lib;

import java.io.*;
import java.util.*;
import static lib.Debug.dp;

public class Model {

	// The min and max lengths of a name.
	private final int minLength;
	private final int maxLength;
	
	// The order of the model.
	private final int order;
	
	// The actual model.
	private HashMap<String, ProbabilityMap> model;
	
	/** A constructor.
	 * 
	 * @param ord - the order of the new model.
	 * @param min - the min name length for the model.
	 * @param max - the max name length for the model.
	 */
	public Model(int ord, int min, int max) {
		this.order = ord;
		this.minLength = min;
		this.maxLength = max;
		this.model = new HashMap<String, ProbabilityMap>();
	}
	
	/** Adding a data-point to our model.
	 * 
	 * @param s - the string that precedes our data point.
	 * @param c - the character at our data point.
	 */
	public void add(String s, Character c) {
		// Make sure the key is valid.
		if(s.length() > order) throw new IllegalStateException("ERROR: Attempting to add a key of length greater than the model's order.");
		
		// If the model doesn't have our key, add it.
		if(!model.containsKey(s)) {
			model.put(s, new ProbabilityMap());
		}
		
		// Update the probability map.
		model.get(s).add(c);
	}
	
	/** Given a key, roll for a character.
	 * 
	 * @param s - the key we want to roll for.
	 * @return - the character we rolled.
	 */
	public Character rollCharacter(String s) {
		// Make sure the key is valid.
		if(s.length() > order) throw new IllegalStateException("ERROR: Attempting to add a key of length greater than the model's order.");
		
		// Roll a character.
		if(!model.containsKey(s)) {
			dp("\tModel does not contain key: '" + s + "'.");
			return ' ';
		}
		return model.get(s).rollCharacter();
	}
	
	/** Generate a name (which may not be the right length) from our model.
	 * 
	 * @return - the name generated.
	 */
	private String generateOnce() {
		String out = " ";
		while(out.length() < order) {
			out += rollCharacter(out);
		}
		dp("\tSeed value: '" + out + "'.");
		while(out.charAt(out.length() - 1) != ' ') {
			String key = out.substring(out.length() - order);
			out += rollCharacter(key);
		}
		return out;
	}
	
	/** Generate names until we get one of a valid length.
	 * 
	 * @return - the name we generated.
	 */
	public String generate() {
		dp("Generating name . . .");
		String out;
		do {
			out = generateOnce();
			dp("Name generated: '" + out + "'.");
		} while (out.length() < minLength || out.length() > maxLength);
		dp("Generated name was valid. Returning . . .");
		return out;
	}

	/** Train on a name.
	 * 
	 * @param name - the name to train on.
	 */
	private void trainOnName(String name) {
		dp("Training on name: " + name);
		
		String key = " ";
		char value;
		
		value = name.charAt(0);
		dp("Key: '" + key + "'\tValue: '" + value + "'.");
		add(key, value);
		
		dp("Was the key added successfully?\n\t" +  model.containsKey(" "));
		
		// For each character C in our name,
		for(int i = 0; i < name.length(); i++) {
			// Append C to S, and trim S if it's too long.
			key += name.charAt(i);
			if(key.length() > order) key = key.substring(1);
			
			// Get our value. (Next char in string, or space if no such char exists.)
			if(i < name.length() - 1) value = name.charAt(i + 1);
			else value = ' ';
			
			dp("Key: " + key + "\tValue: " + value);
			
			// Add key S and character C+1 to our model.
			add(key, value);
		}
	}
	
	/** Train on a file. Accepts files of FIRST NAMES,
	 * 	delimited only by NEW LINEs.
	 * 
	 * @param filename - the name of the file to use.
	 */
	public void train(String filename) {
		dp("Running 'train' method . . .");
		
		File f = null;
		BufferedReader b = null;
		String n = "";
		
		dp("Entering try block . . .");
		
		try {
			dp("Opening file . . . ");
			
			// Open our file.
			f = new File(filename);
			b = new BufferedReader(new FileReader(f));
			
			dp("Opened file.");
			
			// For each name in the file, train.
			while((n = b.readLine()) != null) {
				dp("Attempting to train on name: " + n);
				trainOnName(n);
			}
			
			// Close the file.
			b.close();
			
			dp("Finished training.");
		} catch (Exception e) {
			System.out.println("ERROR: Unable to open file.");
			e.printStackTrace();
			throw new IllegalStateException();
		}
	}
	
	public String toString() {
		String out = "";
		for(String key : model.keySet()) {
			out += "Key: '" + key + "' Probability Map:\n";
			out += model.get(key).toString();
		}
		return out;
	}
}
