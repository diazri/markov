package lib;

import java.util.*;
import static lib.Debug.dp;

public class ProbabilityMap {
	
	// The map between characters and their likelihood.
	private HashMap<Character, Integer> map;
	
	// The number of characters that have been added to this map.
	private int count;

	/** A constructor.
	 */
	public ProbabilityMap() {
		this.map = new HashMap<Character, Integer>();
	}
	
	/** Add a character to this map.
	 * 
	 * @param c - the character to add.
	 */
	public void add(Character c) {
		dp("\tAdding character: " + c + " to probability map.");
		if(map.containsKey(c)) {
			int increment = map.get(c) + 1;
			map.put(c, increment);
		} else {
			map.put(c, 1);
		}
		count++;
	}
	
	/** Get the probability of a character.
	 * 
	 * @param c - the character we want.
	 * @return - the probability of that character being chosen.
	 */
	public double getProbability(Character c) {
		if(!map.containsKey(c)) return -1;
		else {
			double numerator = map.get(c);
			double denominator = count;
			return numerator / denominator;
		}
	}
	
	public Character rollCharacter() {
		dp("\tRolling character . . .");
		// Roll a random number between 0 and 1.
		double roll = Math.random();
		
		// A value to test our probability against.
		double p = 1;
		
		// For each character in our keyset,
		for(Character c : map.keySet()) {
			// Lower the threshold by the probability of hitting this character.
			p -= getProbability(c);
			// If the roll is higher than the threshold, return our character.
			if(roll >= p) {
				dp("Rolled character: " + c);
				return c;
			}
		}
		
		// If we've rolled exactly 0 (or if the map is empty), return a blank space.
		return ' ';
	}
	
	public String toString() {
		String out = "";
		for(Character key : map.keySet()) {
			out += "\t" + "Char: '" + key + "' Probability: " + getProbability(key) + "\n";
		}
		return out;
	}
}
